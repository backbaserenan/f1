# Formula One Renan's test

## Instalation

- clone the repo or unzip.
- npm install.

## Running the App
- npm run start

## Running tests
- ./node_modules/protractor/bin/webdriver-manager update
- ./node_modules/protractor/bin/webdriver-manager start
- npm run test 

## You can see the App online here

[Formula One - Renan](http://198.211.118.18/)



## Short explanation

I decided to keep the app as simple as possible, because simple code is important.

There are two controllers in the app that are responsable keep the views updated and reflecting the correct data.
 - Champion controller, basicaly receives the champions data from a factory service responsable for requests.
 - Season controller, basicaly receives the seasons data from a factory service responsable for requests.
 	(They look the same but aren't, they handle different data)


 Note : The app will not request for champions data more than once. It has a little and simple cache to avoid unnecessary requests. (I'd like to discuss about it later)
 

I avoided to touch $scope because it could make an app slower, and I also decided to create a factory service to handle comunication with the service, this way I can reuse the same service and share data with the controllers and avoid touch $scope and also keep logic independent of views. My next step would be move the controller logic to the service. 

There's a grunt file just for bundle everything using browserify, and also for dev server + less tasks 

Looking forward to talk with you guys.
