'use strict'

var gulp = require('gulp')
var path = require('path')
var less = require('gulp-less')
var concat = require('gulp-concat')
var connect = require('gulp-connect')
var sourcemaps = require('gulp-sourcemaps')
var browserify = require('gulp-browserify')
 
gulp.task('build', function() {
	gulp.src('www/app/index.js')
		.pipe(browserify({
		  insertGlobals : true,
		  debug : !gulp.env.production
		}))
		.pipe(gulp.dest('www/dist/js'))
})

gulp.task('less', function () {
  return gulp.src('www/app/style/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('www/dist/css'))
})

gulp.task('webserver', function() {
  connect.server({
     root: 'www',
  })
})


gulp.task('run', ['less', 'build', 'webserver', 'watch'])

gulp.task('watch', function () {
  gulp.watch('www/app/style/*.less', ['less'])
  gulp.watch('www/app/**/*.js', ['build'])
})