'use strict'

var championsOfTheSeason = 'British Grand Prix Round - 9 Total time - 1:31:27.729 Laps - 52 1:37.093 1st Lewis Hamilton - Mercedes'
describe('Seasons', function() {

  beforeEach(function() {
    browser.get('http://localhost:8080/#/season/2015')
  })

  it('Should have the champion property for the winner', function() {
    var el = element.all(by.css('.champion')).evaluate('season').then(function(val) {
      expect(val[0].champion).toEqual(true)
    })   
  })

  it('Should paint in green', function() {
    expect(element(by.css('.champion')).getAttribute('class')).toMatch('thumbnail champion')
  })

})