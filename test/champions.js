'use strict'


var seasons = [ '2015', '2014', '2013', '2012', '2011', '2010', '2009', '2008', '2007', '2006', '2005' ]

describe('Champions', function() {

  beforeEach(function() {
    browser.get('http://localhost:8080/')
  })

  it('Should load seasons since 2005', function() {
    var el = element.all(by.css('.season-year'))
      el.getText().then(function(val) {
      expect(val).toEqual(seasons)
    })  
  })

  it('Should load Lewis Hamilton as champion of 2015', function() {
    var el = element(by.css('.driver'))
      el.getText().then(function(val) {
      expect(val).toEqual('Lewis Hamilton')
    })  
  })

  it('Should load mercedes as costructor of 2015', function() {
    var el = element(by.css('.constructor'))
      el.getText().then(function(val) {
      expect(val).toEqual('Mercedes')
    })  
  })
  
})