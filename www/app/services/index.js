'use strict'

var champions

module.exports = function(app) {
	app.factory('formulaOneService', getData)
}

var getData = function ($http) {
   return {
      getCachedChampion: function (year) {
        var champion 
        if(champions) {
          champions.map(function(race) {
            if(year == race.season) {
              champion = race.DriverStandings[0].Driver.code
            }
          })  
        }
        return champion
      },
      getCachedChampions: function () {
        return champions ? champions : void 0
      },
      getChampions: function() {
        return $http.get('//ergast.com/api/f1/driverStandings/1.json?limit=11&offset=55')
         .then(function(result) {
            champions = result.data.MRData.StandingsTable.StandingsLists
            return result.data.MRData.StandingsTable.StandingsLists
        })
      },
      getSeason: function(season) {
        return $http.get('//ergast.com/api/f1/'+season+'/results/1.json')
          .then(function(result) {
            return result.data.MRData.RaceTable.Races
        })
    }
  }
}