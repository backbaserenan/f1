'use strict'

var flags = require('../util/flags')
module.exports = function(app) {
	app.controller('championsController', function ($scope, formulaOneService) { 
		var championsData = formulaOneService.getCachedChampions()
		if(championsData) {
			$scope.champions = buildData(championsData)
		} else {
			formulaOneService.getChampions().then(function(data) {
				$scope.champions = buildData(data)
			})
		}
	})
}

var buildData = function (data) {
	return data.reduce(reduceData, [])
}

var reduceData = function (current, next, index) {
	var data = {}
	data.season = next.season
	data.driver = next.DriverStandings[0].Driver
	data.driver.points = next.DriverStandings[0].points
	data.driver.totalWins = next.DriverStandings[0].wins
	data.constructor = next.DriverStandings[0].Constructors[0]
	if(Math.abs(index % 2) == 1) {
		data.odd = true
	}
	data.flag = flags[next.DriverStandings[0].Driver.nationality]

	current.push(data)
	return current.sort(function(a, b) {
		return b.season - a.season
	})
}
