'use strict'

var champion
var flags = require('../util/flags')
module.exports = function(app) {
	app.controller('seasonController', function ($scope, formulaOneService, $routeParams) { 
		champion = formulaOneService.getCachedChampion($routeParams.season)
		if(champion) {
			formulaOneService.getSeason($routeParams.season).then(handleData)	
		} else {
			formulaOneService.getChampions().then(function(data) {
				champion = formulaOneService.getCachedChampion($routeParams.season)
				formulaOneService.getSeason($routeParams.season).then(handleData)
			})
		}
		function handleData (data) {
			$scope.seasons = buildData(data)
			$scope.ready = true
		}
	})
}

var buildData = function (data) {
	return data.reduce(reduceData, [])
}

var reduceData = function(current, next, index) {
	var data = {}
	data.raceData = {
		season: next.season,
		round: next.round,
		raceName: next.raceName,
		date: next.date
	}

	data.circuit = next.Circuit
	data.fastestLap = next.Results[0].FastestLap
	data.laps = next.Results[0].laps
	data.driver = next.Results[0].Driver

	if(next.Results[0].Time) {
		data.time = next.Results[0].Time.time
	} else {
		data.time = "unknow"
	} 
	 
	data.constructor = next.Results[0].Constructor
	if(Math.abs(index % 2) == 1) {
		data.odd = true
	}

	if(champion === data.driver.code) {
		data.champion = true
	}
	data.flag = flags[data.circuit.Location.country]

	current.push(data)
	return current
}		