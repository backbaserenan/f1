'use strict'

module.exports = function(app) {
	app.config(routeConfig)
}

var routeConfig = function ($routeProvider) {
	$routeProvider.when("/season/:season", {
		templateUrl: "app/views/season.html",
		controller: "seasonController"
	})

	.when('/', {
     templateUrl:"app/views/champions.html",
     controller:'championsController'
	 })
}