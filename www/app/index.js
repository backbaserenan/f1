'use strict'

var angular = require('angular')
var app = angular.module('formulaOneApp', [require('angular-route'), require('angular-loading-bar')])

require('./controllers/champion')(app)
require('./controllers/season')(app)
require('./services')(app)
require('./routes')(app)


